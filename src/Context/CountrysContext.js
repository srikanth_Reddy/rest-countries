import React, { createContext } from 'react'
import { useState } from 'react';
export const dataContext = createContext();

function CountryContext(props) {
  let [dat, setDat] = useState({
    error: null,
    isLoaded: false,
    countries: []
})
let [countryData, SetCountryData] = useState({})
  return <dataContext.Provider value={{ dat, setDat,countryData,SetCountryData }}>
    {props.children}
  </dataContext.Provider>

}
export default CountryContext
