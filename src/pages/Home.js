import React from 'react'
import { useEffect, useState, useContext, useCallback } from 'react';
import './Home.css'
import { useNavigate } from "react-router-dom"
import { dataContext } from '../Context/CountrysContext';
import { Theme } from '../App'

const Home = () => {

    const navigate = useNavigate();

    const { theme, themeToggle } = useContext(Theme)


    //Global data
    let { dat, setDat, SetCountryData } = useContext(dataContext);

    //To store the searchingString(Country) 
    let [searchStr, setSearchStr] = useState('');

    //To store the Region selected by user
    let [region, setRegion] = useState('');



    // Fetch the countries data based on the region 
    useEffect(() => {
        ((region === "") ? fetch("https://restcountries.com/v2/all") : fetch(`https://restcountries.com/v2/region/${region}`))
            .then((res) => res.json())
            .then((result) =>
                setDat({
                    isLoaded: true,
                    countries: result

                }),
                (error) => setDat({ isLoaded: true, error })
            )
    }, [region, setDat])



    // Filter the data based on the SearchingString(Countries)
    let filter = useCallback((data) => {
        return data.filter((country) => {
            if (((country.name).toLowerCase()).includes(searchStr.toLowerCase())) {
                return true;
            }
        })
    }, [searchStr])


    return (<div id={theme}>
        <div className='header'  >

            <div className='title'>
                <h2>Where in the world</h2>
                <div><button onClick={() => themeToggle()}>{(theme === 'light') ? 'Dark Mode' : 'Light Mode'}</button></div>
            </div>
            <div className='filtering'>
                <div> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16"> <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/> </svg> <input onChange={(e) => setSearchStr(e.target.value)} type="text" placeholder='Search for a country...'></input></div>
                <div>
                    <select onChange={(e) => setRegion(e.target.value)}>
                        <option value="">Filter By Region</option>
                        <option value="Africa">Africa</option>
                        <option value="Americas">Americas</option>
                        <option value="Antarctic">Antarctic</option>
                        <option value="Antarctic Ocean">Antarctic Ocean</option>
                        <option value="Asia">Asia</option>
                        <option value="Europe">Europe</option>
                        <option value="Oceania">Oceania</option>
                        <option value="Polar">Polar</option>
                    </select>
                </div>
            </div>
        </div>
        <div className='countries'>
            {filter(dat.countries).map((country) => {
                return <div
                    className='country'
                    key={country.name}
                    onClick={
                        () => {
                            SetCountryData(country);
                            navigate("/country")
                        }
                    }>
                    <img alt="flag" className='flag' src={country.flags.png}></img>
                    <div className='info'>
                        <h3>{country.name}</h3>
                        <div><span>Population :</span> {country.population}</div>
                        <div><span>Region :</span> {country.region}</div>
                        <div><span>Capital :</span> {country.capital}</div>
                    </div>
                </div>
            })}
        </div>
    </div>
    )
}

export default Home;