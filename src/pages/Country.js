import React from 'react'
import { dataContext } from '../Context/CountrysContext';
import { useContext } from 'react';
import { useNavigate } from "react-router-dom"
import './Country.css'
import { Theme } from '../App'


export const Country = () => {

    const { theme, themeToggle } = useContext(Theme)


    let navigate = useNavigate()


    let { countryData } = useContext(dataContext)


    return (<div className="countryPage">
        <div id={theme} >

        <div className='title'>
                <h2>Where in the world</h2>
                <div><button onClick={()=>themeToggle()}>{(theme==='light')?'Dark Mode':'Light Mode'}</button></div>
            </div>

            <button className='btn' onClick={() => navigate(-1)}><span className='arrow'>&#8592;</span>Back</button>

            <div className='singleCountry'>
                <div className='Countryflag'><img alt="flag" src={countryData.flags.png}></img></div>

                <div className='details'>
                    <h1>{countryData.name}</h1>
                    <div className='countryDetails'>
                        <div className='left'>
                            <div><span className='name'> Native Name : </span>{countryData.nativeName} </div>
                            <div><span className='name'> Population : </span>{countryData.population} </div>
                            <div> <span className='name'> Region : </span>{countryData.region} </div>
                            <div> <span className='name'> Sub Region : </span>{countryData.subregion} </div>
                        </div>
                        <div className='right'>
                            <div> <span className='name'> Capital : </span>{countryData.capital} </div>
                            <div><span className='name'> Top Level Domain : </span>{countryData.topLevelDomain[0]} </div>
                            <div> <span className='name'> Currencies : </span>{countryData.currencies.map((cur) => { return <span key={cur.code}>{cur.name},</span> })} </div>
                            <div> <span className='name'> Languages : </span>{countryData.languages.map((lng) => { return <span key={lng.iso639_1}>{lng.name},</span> })} </div>
                        </div>
                    </div>
                    <div>
                        <h4>Border Countries : {(countryData.borders || ["Not avalible"]).map((border) => { return <span className='footer' key={border}>{border}</span> })}</h4>
                    </div>
                </div>

            </div>
        </div>
    </div>
    )
}
