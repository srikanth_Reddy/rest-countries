import React from 'react'
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Home from '../pages/Home'
import { Country } from '../pages/Country'
import Nopage from '../pages/Nopage'
import CountryContext from '../Context/CountrysContext'


const UserRoutes = () => {

  return (

    <BrowserRouter>
      <CountryContext>              
        <Routes>
          <Route path='/' element={<Home />} ></Route>
          <Route path='country' element={<Country />}></Route>
          <Route path="*" element={<Nopage />}></Route>
        </Routes>
      </CountryContext>
    </BrowserRouter>

  )
}

export default UserRoutes;