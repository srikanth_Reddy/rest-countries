import './App.css';
import UserRoutes from './Routes/UserRoutes';
import { createContext, useState } from 'react'

export const Theme = createContext()
function App() {


  let [theme, setTheme] = useState('light');

  let themeToggle = () =>{
    (theme === 'light') ? setTheme("dark") : setTheme("light");
  }


  return (

    <div className="App" id={theme}>
      <Theme.Provider value={{theme,themeToggle}}>
        <UserRoutes />
      </Theme.Provider>

    </div>
  );
}

export default App;
